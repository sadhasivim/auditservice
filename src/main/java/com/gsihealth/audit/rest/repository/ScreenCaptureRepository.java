package com.gsihealth.audit.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gsihealth.audit.rest.entity.ScreenCaptureRecord;

@Repository
public interface ScreenCaptureRepository extends JpaRepository<ScreenCaptureRecord, Long> {

}
