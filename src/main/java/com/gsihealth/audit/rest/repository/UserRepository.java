package com.gsihealth.audit.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.gsihealth.audit.rest.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserId(long id);

	User findByEmail(String email);

	List<User> findAll();

}
