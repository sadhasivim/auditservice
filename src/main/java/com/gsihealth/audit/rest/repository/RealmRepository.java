package com.gsihealth.audit.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.gsihealth.audit.rest.entity.Realm;

@Repository
public interface RealmRepository extends JpaRepository<Realm, Long> {
	List<Realm> findAllByRealmName(String name);

	List<Realm> findAllByName(String name);

	List<Realm> findAll();
}
