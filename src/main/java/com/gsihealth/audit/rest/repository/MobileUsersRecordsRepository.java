package com.gsihealth.audit.rest.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.gsihealth.audit.rest.entity.MobileUsersRecords;

@Repository
public interface MobileUsersRecordsRepository extends JpaRepository<MobileUsersRecords, Long> {

	MobileUsersRecords findRecordWithoutDateOut(long id);

	List<MobileUsersRecords> findRecordsWithoutDateOut(long id);

	List<MobileUsersRecords> findRecordBetweenDate(long commId, Date date1, Date date2);
}
