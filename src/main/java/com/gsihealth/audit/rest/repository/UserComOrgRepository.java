package com.gsihealth.audit.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.gsihealth.audit.rest.entity.UserComOrg;

@Repository
public interface UserComOrgRepository extends JpaRepository<UserComOrg, Long> {

	UserComOrg findByUserCommunityOrganizationId(long id);

	UserComOrg findUserByAddressComId(long comId, String directAddress);

	List<UserComOrg> findAllByRealmId(long id);

	List<UserComOrg> findGroups(long id);

	List<UserComOrg> findUsersByGroups(long id, long id2);
}
