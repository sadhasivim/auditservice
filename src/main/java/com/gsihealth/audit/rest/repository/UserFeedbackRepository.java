package com.gsihealth.audit.rest.repository;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gsihealth.audit.authentication.BadTokenException;
import com.gsihealth.audit.authentication.IDBRestClient;
import com.gsihealth.audit.rest.entity.UserFeedback;
import com.gsihealth.audit.rest.MobileConfig;

@Repository
@Transactional(MobileConfig.TX_MANAGER)
public class UserFeedbackRepository {
	static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	 private static final String prefix = "http://";
	 private String idsuffix = "/identity/";
	 private @Autowired @Qualifier(MobileConfig.ENTITY_MANAGER) EntityManager em;

	public void createUserFeedback(@PathParam("token") String token, @PathParam("idpath") String idpath, UserFeedback entity) throws Exception {
		
		if (tokenOkay(idpath, token)) {
		try {
			em.persist(entity);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "CREATE UserFeedback FAILED");
			ex.printStackTrace();
			throw new Exception("CREATE UserFeedback FAILED");
		}
		}
		 else{
			 System.out.println("CREATE UserFeedback bad token");
	            throw new BadTokenException();
		 }
	}
	
	
	
	private boolean tokenOkay(String idpath, String token) {
        if (idpath.indexOf("XXX") >= 0) {
            idpath = idpath.replace("XXX", "/");
        }else{
            idpath = idpath + "/openam";
        }
        idsuffix = "/identity/";
        String url = prefix + idpath + idsuffix + "isTokenValid";
        System.out.println("token check " + url + " " + token);
        IDBRestClient rc = new IDBRestClient();
        Boolean test = false;
        try {
            test = rc.validateToken(token, url);
        } catch (Exception x) {
            System.out.println("COULD NOT VALIDATE TOKEN on path " + idpath);
        }
        return test;
    }

}
