package com.gsihealth.audit.rest.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.gsihealth.audit.rest.entity.LoginRecords;

@Repository
public interface LoginRecordsRepository extends JpaRepository<LoginRecords, Long> {
	List<LoginRecords> findRecordBetweenDate(long commId, Date date1, Date date2);

	LoginRecords findRecordByRecordId(long recordId);
}
