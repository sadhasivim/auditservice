package com.gsihealth.audit.rest;

import java.io.IOException;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.web.client.RestTemplate;

import com.gsihealth.springboot.utils.GsiSpringBootConfig;

@Configuration
@ComponentScan(basePackages = { "com.gsihealth.audit.rest.*" })
@EntityScan(value = "com.gsihealth.audit.rest.entity")
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
@EnableJpaRepositories(value = "com.gsihealth.audit.rest.repository", entityManagerFactoryRef = MobileConfig.ENTITY_MANAGER, transactionManagerRef = MobileConfig.TX_MANAGER)
//@PropertySource(ignoreResourceNotFound = true, value = "classpath:application-${spring.profiles.active}.properties")
public class MobileConfig extends GsiSpringBootConfig {

	public static final String ENTITY_MANAGER = "mobileEntityManager";
	public static final String TX_MANAGER = "mobileTransactionManager";

	@Autowired(required = false)
	private PersistenceUnitManager persistenceUnitManager;

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		return factory;
	}

	@Bean
	@Primary
	@ConfigurationProperties("app.db.mobile.jpa")
	public JpaProperties mobileJpaAdapterProperties() {
		return new JpaProperties();
	}

 	@Bean
    @Primary
    public DataSource mobDataSource() throws ClassNotFoundException, IOException {
    	return super.mobileDataSource();
    } 

	@Bean
	@Primary
	public LocalContainerEntityManagerFactoryBean mobileEntityManager(JpaProperties mobileJpaAdapterProperties) throws ClassNotFoundException, IOException {
		EntityManagerFactoryBuilder builder = createEntityManagerFactoryBuilder(mobileJpaAdapterProperties);
		return builder.dataSource(mobDataSource()).packages("com.gsihealth.audit.rest.entity")
				.persistenceUnit("mobilePU").build();
	}

	@Bean
	public JpaTransactionManager mobileTransactionManager(EntityManagerFactory mobileEntityManager) {
		return new JpaTransactionManager(mobileEntityManager);
	}

	private EntityManagerFactoryBuilder createEntityManagerFactoryBuilder(JpaProperties customerJpaProperties) {
		JpaVendorAdapter jpaVendorAdapter = createJpaVendorAdapter(customerJpaProperties);
		return new EntityManagerFactoryBuilder(jpaVendorAdapter, customerJpaProperties.getProperties(),
				this.persistenceUnitManager);
	}

	private JpaVendorAdapter createJpaVendorAdapter(JpaProperties jpaProperties) {
		AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setShowSql(jpaProperties.isShowSql());
		adapter.setDatabase(jpaProperties.getDatabase());
		adapter.setDatabasePlatform(jpaProperties.getDatabasePlatform());
		adapter.setGenerateDdl(jpaProperties.isGenerateDdl());
		return adapter;
	}

	@Bean
	public InstrumentationLoadTimeWeaver loadTimeWeaver() throws Throwable {
		InstrumentationLoadTimeWeaver loadTimeWeaver = new InstrumentationLoadTimeWeaver();
		return loadTimeWeaver;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
