package com.gsihealth.audit.rest;

import java.io.IOException;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import com.gsihealth.springboot.utils.GsiSpringBootConfig;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.gsihealth.audit.rest.*" })
@EntityScan(basePackages = { "com.gsihealth.audit.rest.entity" })
@EnableJpaRepositories(value = "com.gsihealth.audit.rest.repository",entityManagerFactoryRef = ConnectConfig.ENTITY_MANAGER, transactionManagerRef = ConnectConfig.TX_MANAGER)
public class ConnectConfig extends GsiSpringBootConfig {

	public static final String ENTITY_MANAGER = "connectEntityManager";
	public static final String TX_MANAGER = "connectTransactionManager";

	@Autowired(required = false)
	private PersistenceUnitManager persistenceUnitManager;

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		return factory;
	}

	@Bean
	@ConfigurationProperties("app.db.connect.jpa")
	public JpaProperties connectJpaAdapterProperties() {
		return new JpaProperties();
	}

	@Bean
	public DataSource connectDataSource() throws ClassNotFoundException, IOException {
		return super.connectDataSource();
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean connectEntityManager(JpaProperties connectJpaAdapterProperties) throws ClassNotFoundException, IOException {
		EntityManagerFactoryBuilder builder = createEntityManagerFactoryBuilder(connectJpaAdapterProperties);
		return builder.dataSource(connectDataSource()).packages("com.gsihealth.audit.rest.entity")
				.persistenceUnit("connectPU").build();
	}

	@Bean
	public JpaTransactionManager connectTransactionManager(EntityManagerFactory connectEntityManager) {
		return new JpaTransactionManager(connectEntityManager);
	}

	private EntityManagerFactoryBuilder createEntityManagerFactoryBuilder(JpaProperties customerJpaProperties) {
		JpaVendorAdapter jpaVendorAdapter = createJpaVendorAdapter(customerJpaProperties);
		return new EntityManagerFactoryBuilder(jpaVendorAdapter, customerJpaProperties.getProperties(),
				this.persistenceUnitManager);
	}

	private JpaVendorAdapter createJpaVendorAdapter(JpaProperties jpaProperties) {
		AbstractJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setShowSql(jpaProperties.isShowSql());
		adapter.setDatabase(jpaProperties.getDatabase());
		adapter.setDatabasePlatform(jpaProperties.getDatabasePlatform());
		adapter.setGenerateDdl(jpaProperties.isGenerateDdl());
		return adapter;
	}

	@Bean
	public InstrumentationLoadTimeWeaver loadTimeWeaver() throws Throwable {
		InstrumentationLoadTimeWeaver loadTimeWeaver = new InstrumentationLoadTimeWeaver();
		return loadTimeWeaver;
	}

}