package com.gsihealth.audit.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsihealth.audit.rest.entity.UserFeedback;
import com.gsihealth.audit.rest.repository.UserFeedbackRepository;

@RestController
@RequestMapping("/mobile/userFeedback")
public class UserFeedbackController extends TokenController {

	@Autowired
	private UserFeedbackRepository userFeedbackRepository;

	@RequestMapping(value = "/{idpath}/save", method = RequestMethod.POST)
	@Consumes({ "application/json" })
	public void create(HttpServletRequest request, @PathVariable("idpath") String idpath, HttpEntity<String> httpEntity)
			throws Exception {
		String jwtToken = request.getHeader("token");
		String token = getTokenFromJWT(jwtToken);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String body = httpEntity.getBody();
		UserFeedback userFeedback = mapper.readValue(body, UserFeedback.class);
		userFeedbackRepository.createUserFeedback(token, idpath, userFeedback);
	}

}