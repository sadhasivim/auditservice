package com.gsihealth.audit.rest.controller;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.gsihealth.audit.rest.entity.LoginRecords;
import com.gsihealth.audit.rest.entity.MobileUsersRecords;
import com.gsihealth.audit.rest.entity.UserComOrg;
import com.gsihealth.audit.rest.repository.LoginRecordsRepository;
import com.gsihealth.audit.rest.repository.MobileUsersRecordsRepository;
import com.gsihealth.audit.rest.repository.UserComOrgRepository;

@RestController
@RequestMapping("auditService")
public class AuditServiceController {

	@Autowired
	LoginRecordsRepository loginRecRepo;

	@Autowired
	UserComOrgRepository userComOrgRepository;

	@Autowired
	MobileUsersRecordsRepository mobileUserRecRepo;

	@RequestMapping(value = "/add-login-record", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public String addLoginRecord(@RequestHeader("userDirectAddress") String userDirectAddress,
			@RequestHeader("OS") String os, HttpServletRequest request)
			throws Exception {
		long commId = Long.parseLong(request.getHeader("commId"));
		try {
			UserComOrg uco = userComOrgRepository.findUserByAddressComId(commId, userDirectAddress);
			if (uco.getUser() != null) {
				LoginRecords lr = new LoginRecords();
				long t1 = System.currentTimeMillis();
				Timestamp currentDate = new Timestamp(t1);
				lr.setUserId(uco.getUser().getUserId());
				lr.setCommId(uco.getRealmId());
				lr.setSignedInDate(currentDate);
				lr.setOs(os);
				loginRecRepo.save(lr);
				Gson gson = new Gson();
				String jsonString = gson.toJson(lr);
				return jsonString;
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return null;
	}

	@RequestMapping(value = "/check-user-mobile-signin", method = RequestMethod.GET)
	public void checkIfUserHasSignedIn(@RequestHeader("userDirectAddress") String userDirectAddress,HttpServletRequest request) throws Exception {
		long commId = Long.parseLong(request.getHeader("commId"));
		try {
			UserComOrg uco = userComOrgRepository.findUserByAddressComId(commId, userDirectAddress);
			if (uco.getUser() != null) {
				MobileUsersRecords mur1 = mobileUserRecRepo.findRecordWithoutDateOut(uco.getUser().getUserId());
				if (mur1 != null && !mur1.getHasLoggedIn()) {
					mur1.setHasLoggedIn(true);
					mobileUserRecRepo.save(mur1);
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

	@RequestMapping(value = "/update-login-record", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> updateLoginRecordAudit(HttpEntity<String> httpEntity) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		try {
			String json = httpEntity.getBody();
			JSONObject jsonObj = new JSONObject(json);
			LoginRecords lr = loginRecRepo.findRecordByRecordId(jsonObj.getInt("recordId"));
			System.out.println(jsonObj.getInt("accessedAlerts"));
			lr.setAccessedAlerts(jsonObj.getInt("accessedAlerts"));
			lr.setAccessedMessages(jsonObj.getInt("accessedMessages"));
			lr.setAccessedCarebooklite(jsonObj.getInt("accessedCarebooklite"));
			lr.setAccessedTasking(jsonObj.getInt("accessedTasking"));

			loginRecRepo.save(lr);

			String jsonInString = mapper.writeValueAsString(lr);

			return new ResponseEntity<String>(jsonInString, HttpStatus.OK);

		} catch (Exception ex) {
			System.out.println("exception " + ex.toString());
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

	}

}
