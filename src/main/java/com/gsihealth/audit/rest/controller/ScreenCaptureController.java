package com.gsihealth.audit.rest.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gsihealth.audit.rest.entity.LoginRecords;
import com.gsihealth.audit.rest.entity.ScreenCaptureRecord;
import com.gsihealth.audit.rest.repository.ScreenCaptureRepository;

@RestController
@RequestMapping("auditService/mobileScreen/captureDetails")
public class ScreenCaptureController extends TokenController {

	@Autowired
	private ScreenCaptureRepository screenCaptureRepo;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@Consumes({ "application/json" })
	public String create(HttpServletRequest request, HttpEntity<String> httpEntity) throws Exception {
		String body = httpEntity.getBody();

		try {
			ScreenCaptureRecord scr = new ScreenCaptureRecord();
			JSONObject jsonObj = new JSONObject(body);
			LoginRecords lr = new LoginRecords();
			lr.setRecordId(jsonObj.getLong("loginRecord"));
			scr.setLoginRecord(lr);
			scr.setCapturedScreen(jsonObj.getString("capturedScreen"));
			scr.setPatientId(jsonObj.getLong("patientId"));
			scr.setCapturedTime(new Date());
			screenCaptureRepo.save(scr);
			return "Record saved successfully";
		} catch (Exception e) {
			System.out.println("Error Occured: " + e);
			return "Error Occured";
		}

	}

}