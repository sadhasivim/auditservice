package com.gsihealth.audit.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.gsihealth.jwt.JWT;
import com.gsihealth.springboot.utils.HeaderService;

public class TokenController {
	@Autowired
	private HeaderService headerService;
	private static final String key = "alcatraz";
	protected String getTokenFromJWT(String jwtToken) throws Exception
	{
		StringBuilder openamToken = new StringBuilder();
				try {
					StringBuilder dummy = new StringBuilder();
					new JWT().parseJWT(jwtToken, key, openamToken);
				} catch (Exception x) {
					throw new Exception("BAD TOKEN");
				}
				String token = openamToken.toString();
				return token;
	}
	
	protected String getTokenFromJWT()
	{
		return headerService.getOpenAmToken();
	}
	protected long getCommunityId()
	{
		return headerService.getCommunityId();
	}
	protected long getUserId()
	{
		return headerService.getUserId();
	}
}
