package com.gsihealth.audit.rest.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@NamedQuery(name = "LoginRecords.findRecordBetweenDate", query = "SELECT l FROM LoginRecords l WHERE l.commId=?1 AND l.signedInDate >= ?2 AND l.signedInDate<=?3")
@Table(name = "user_logins_records", catalog = "mobile")
public class LoginRecords implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "record_id")
	private long recordId;
	@Column(name = "community_id")
	private long commId;
	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
	@JsonManagedReference
	private User user;
	@Column(name = "user_id")
	private long userId;

	public long getUserId() {
		return userId;
	}

	@Column(name = "accessed_messages")
	private Integer accessedMessages;
	@Column(name = "accessed_carebooklite")
	private Integer accessedCarebooklite;
	@Column(name = "accessed_alerts")
	private Integer accessedAlerts;
	@Column(name = "accessed_tasking")
	private Integer accessedTasking;
	@Column(name = "OS")
	private String os;

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public Integer getAccessedMessages() {
		return accessedMessages;
	}

	public void setAccessedMessages(Integer accessedMessages) {
		this.accessedMessages = accessedMessages;
	}

	public Integer getAccessedCarebooklite() {
		return accessedCarebooklite;
	}

	public void setAccessedCarebooklite(Integer accessedCarebooklite) {
		this.accessedCarebooklite = accessedCarebooklite;
	}

	public Integer getAccessedAlerts() {
		return accessedAlerts;
	}

	public void setAccessedAlerts(Integer accessedAlerts) {
		this.accessedAlerts = accessedAlerts;
	}

	public Integer getAccessedTasking() {
		return accessedTasking;
	}

	public void setAccessedTasking(Integer accessedTasking) {
		this.accessedTasking = accessedTasking;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Column(name = "signed_in_date")
	private Timestamp signedInDate;

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getCommId() {
		return commId;
	}

	public void setCommId(long commId) {
		this.commId = commId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getSignedInDate() {
		return signedInDate;
	}

	public void setSignedInDate(Timestamp signedInDate) {
		this.signedInDate = signedInDate;
	}
}
