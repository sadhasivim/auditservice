package com.gsihealth.audit.rest.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonRootName;

@Entity
@IdClass(RealmIdClass.class)
@Table(name = "configuration", catalog = "connect")
@JsonRootName(value = "Realm")
public class Realm implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "community_id")
	private Long realmId;
	@Id
	@Column(name = "name")
	private String name;
	@Column(name = "value")
	private String realmName;

	public Long getRealmId() {
		return realmId;
	}

	public void setRealmId(Long realmId) {
		this.realmId = realmId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRealmName() {
		return realmName;
	}

	public void setRealmName(String realmName) {
		this.realmName = realmName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
