/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.audit.rest.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hemant Tyagi
 */
@Entity
@Table(name = "user_feedback", catalog = "mobile", schema = "")
@XmlRootElement(name = "UserFeedback")
@JsonRootName(value = "UserFeedback")
@NamedQueries({
    @NamedQuery(name = "UserFeedback.findAll", query = "SELECT u FROM UserFeedback u"),
    @NamedQuery(name = "UserFeedback.findById", query = "SELECT u FROM UserFeedback u WHERE u.id = :id"),
    @NamedQuery(name = "UserFeedback.findByUserName", query = "SELECT u FROM UserFeedback u WHERE u.userName = :userName"),
    @NamedQuery(name = "UserFeedback.findByCommunityOid", query = "SELECT u FROM UserFeedback u WHERE u.communityOid = :communityOid"),
    @NamedQuery(name = "UserFeedback.findByRating", query = "SELECT u FROM UserFeedback u WHERE u.rating = :rating"),
    @NamedQuery(name = "UserFeedback.findByCreationDateTime", query = "SELECT u FROM UserFeedback u WHERE u.creationDateTime = :creationDateTime"),
    @NamedQuery(name = "UserFeedback.findByText", query = "SELECT u FROM UserFeedback u WHERE u.text = :text")})
public class UserFeedback implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Size(max = 100)
    @Column(name = "USER_NAME")
    private String userName;
    @Size(max = 100)
    @Column(name = "COMMUNITY_OID")
    private String communityOid;
    @Column(name = "RATING")
    private Integer rating;
    @Column(name = "CREATION_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDateTime;
    @Size(max = 400)
    @Column(name = "TEXT")
    private String text;

    public UserFeedback() {
    }

    public UserFeedback(Long id) {
        this.id = id;
    }

    public UserFeedback(Long id, Date creationDateTime) {
        this.id = id;
        this.creationDateTime = creationDateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommunityOid() {
        return communityOid;
    }

    public void setCommunityOid(String communityOid) {
        this.communityOid = communityOid;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Date getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserFeedback)) {
            return false;
        }
        UserFeedback other = (UserFeedback) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "UserFeedback [id=" + id + "]";
	}
    
}
