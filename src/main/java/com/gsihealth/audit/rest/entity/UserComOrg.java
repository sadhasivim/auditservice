package com.gsihealth.audit.rest.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonRootName;

//SELECT ORG_ID, COUNT(USER_ID) AS users_in_group FROM user_community_organization WHERE community_id=2 GROUP BY ORG_ID
@Entity
@Table(name = "user_community_organization", catalog = "connect")
@JsonRootName(value = "UserComOrg")
@NamedQueries({
		@NamedQuery(name = "UserComOrg.findUserGroupCount", query = "SELECT u.organization.orgId as org, count(u.user.userId) as occurances FROM UserComOrg u WHERE u.realmId = ?1 GROUP BY u.organization.orgId "),
		@NamedQuery(name = "UserComOrg.findGroups", query = "SELECT u FROM UserComOrg u WHERE u.realmId = ?1 GROUP BY u.organization.orgId "),
		@NamedQuery(name = "UserComOrg.findUsersByGroups", query = "SELECT u FROM UserComOrg u WHERE u.realmId = ?1 AND u.organization.orgId =?2 ORDER BY u.user.creationDate DESC"),
		@NamedQuery(name = "UserComOrg.findUserByAddressComId", query = "SELECT u FROM UserComOrg u WHERE u.realmId = ?1 AND u.directAddress =?2") })

public class UserComOrg implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_community_organization_id")
	private long userCommunityOrganizationId;

	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
	private User user;
	@Column(name = "DIRECT_ADDRESS")
	private String directAddress;

	public String getDirectAddress() {
		return directAddress;
	}

	public void setDirectAddress(String directAddress) {
		this.directAddress = directAddress;
	}

	@Column(name = "community_id")
	private long realmId;

	public long getRealmId() {
		return realmId;
	}

	public void setRealmId(long realmId) {
		this.realmId = realmId;
	}

	@Transient
	private Realm realm;
	@OneToOne
	@JoinColumn(name = "ORG_ID", referencedColumnName = "ORG_ID", insertable = false, updatable = false)
	@JsonManagedReference(value = "orgRef")
	private Organization organization;

	public long getUserCommunityOrganizationId() {
		return userCommunityOrganizationId;
	}

	public void setUserCommunityOrganizationId(long userCommunityOrganizationId) {
		this.userCommunityOrganizationId = userCommunityOrganizationId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Realm getRealm() {
		return realm;
	}

	public void setRealm(Realm realm) {
		this.realm = realm;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
