package com.gsihealth.audit.rest.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "mobile_user_records", catalog = "connect")
@NamedQueries({
		@NamedQuery(name = "MobileUsersRecords.findRecordWithoutDateOut", query = "SELECT m FROM MobileUsersRecords m WHERE m.dateOut = NULL AND m.userId=?1"),
		@NamedQuery(name = "MobileUsersRecords.findRecordBetweenDate", query = "SELECT m FROM MobileUsersRecords m WHERE m.dateOut = NULL AND m.commId=?1 AND m.dateIn >= ?2 AND m.dateIn<=?3"),
		@NamedQuery(name = "MobileUsersRecords.findRecordsWithoutDateOut", query = "SELECT m FROM MobileUsersRecords m WHERE m.dateOut = NULL AND m.commId=?1") })
public class MobileUsersRecords implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "record_id")
	private long recordId;
	@Column(name = "community_id")
	private long commId;
	@Column(name = "USER_ID")
	private long userId;
	@Column(name = "DATE_IN")
	private Timestamp dateIn;
	@Column(name = "DATE_OUT")
	private Timestamp dateOut;
	@Column(name = "has_logged_in")
	private Boolean hasLoggedIn;

	public Boolean getHasLoggedIn() {
		return hasLoggedIn;
	}

	public void setHasLoggedIn(Boolean hasLoggedIn) {
		this.hasLoggedIn = hasLoggedIn;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getCommId() {
		return commId;
	}

	public void setCommId(long commId) {
		this.commId = commId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Timestamp getDateIn() {
		return dateIn;
	}

	public void setDateIn(Timestamp dateIn) {
		this.dateIn = dateIn;
	}

	public Timestamp getDateOut() {
		return dateOut;
	}

	public void setDateOut(Timestamp dateOut) {
		this.dateOut = dateOut;
	}

}
