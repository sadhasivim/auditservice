package com.gsihealth.audit.rest.entity;

import java.sql.Timestamp;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class OrgCountHelper {
	@SerializedName("orgId")
	private Long orgId;
	@SerializedName("orgName")
	private String orgName;
	@SerializedName("users")
	private List<String> users;
	@SerializedName("userIds")
	private List<Long> userIds;
	@SerializedName("count")
	private int count;
	@SerializedName("joinedDates")
	private List<Timestamp> joinedDates;
	@SerializedName("userCreationDate")
	private List<Timestamp> userCreationDate;

	public List<Timestamp> getUserCreationDate() {
		return userCreationDate;
	}

	public void setUserCreationDate(List<Timestamp> userCreationDate) {
		this.userCreationDate = userCreationDate;
	}

	public List<Timestamp> getJoinedDates() {
		return joinedDates;
	}

	public void setJoinedDates(List<Timestamp> joinedDates) {
		this.joinedDates = joinedDates;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users = users;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

}
