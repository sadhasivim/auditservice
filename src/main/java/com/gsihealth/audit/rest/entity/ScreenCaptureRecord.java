package com.gsihealth.audit.rest.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "screen_capture_record", catalog = "mobile")
public class ScreenCaptureRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@ManyToOne
	@JoinColumn(name = "login_record_id")
	private LoginRecords loginRecord;

	@Column(name = "captured_screen")
	private String capturedScreen;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "captured_time")
	private Date capturedTime;

	@Column(name = "patient_id")
	private long patientId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCapturedScreen() {
		return capturedScreen;
	}

	public void setCapturedScreen(String capturedScreen) {
		this.capturedScreen = capturedScreen;
	}

	public Date getCapturedTime() {
		return capturedTime;
	}

	public void setCapturedTime(Date capturedTime) {
		this.capturedTime = capturedTime;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LoginRecords getLoginRecord() {
		return loginRecord;
	}

	public void setLoginRecord(LoginRecords loginRecord) {
		this.loginRecord = loginRecord;
	}

}
