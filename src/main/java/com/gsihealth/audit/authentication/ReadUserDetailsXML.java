/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.audit.authentication;



public class ReadUserDetailsXML {

	/*
	 * private ResourceBundle dbConfig = ResourceBundle
	 * .getCustomBundle("customAttributes");
	 */
	private String value;

	public ReadUserDetailsXML(String value) {
		this.value = value;

	}

	/*
	 * private String readFileAsString(String filePath) throws
	 * java.io.IOException { BufferedReader reader = new BufferedReader(new
	 * FileReader(filePath)); String line, results = ""; while ((line =
	 * reader.readLine()) != null) { results += line; } reader.close(); return
	 * results; }
	 */

	/*
	 * public static void main(String[] args) throws Exception,
	 * ParserConfigurationException, SAXException { UserDetail detail =
	 * getUserDetail(); System.out.println("Community : " +
	 * detail.getCommunity()); }
	 */

	private void setValueToBean(String attName, String value) throws Exception {
	/*	long communityId = Long.valueOf(dbConfig.getProperty(SharedConstants.dashCommunityId));
		if (attName.equalsIgnoreCase(dbConfig.getString("uid"))) {
			// userDetail.setUid(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("organizationname"))) {
			// userDetail.setOrganizationname(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("mail"))) {
			userDetail.setEmail(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("sn"))) {
			userDetail.setMiddleName(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("community"))) {
			// userDetail.setCommunityId(Long.parseLong(value));
		} else if (attName.equalsIgnoreCase(dbConfig.getString("givenname"))) {
			userDetail.setFirstName(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("lastupdated"))) {
			userDetail.setLastUpdateDate(new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy").parse(value));
		} else if (attName.equalsIgnoreCase(dbConfig.getString("telephonenumber"))) {
			userDetail.setHomePhone(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("healthhomerole"))) {
			userDetail.setRole(new Role(new RoleId(Long.parseLong(value), communityId)));
		} else if (attName.equalsIgnoreCase(dbConfig.getString("signingcredentials"))) {
			userDetail.setCredencials(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("organizationid"))) {
			// userDetail.setOrganizationid(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("userpassword"))) {
			// userDetail.setUserpassword(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("accesslevel"))) {
			userDetail.setAccessLevel(new AccessLevel(new AccessLevelId(Long.parseLong(value), communityId)));
		} else if (attName.equalsIgnoreCase(dbConfig.getString("telephoneextension"))) {
			userDetail.setPhoneExt(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("cn"))) {
			userDetail.setLastName(value.split(" ")[value.split(" ").length - 1]);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("customgroups"))) {
			// userDetail.setCustomgroups(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("inetuserstatus"))) {
			userDetail.setStatus(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("middleinitial"))) {
			// userDetail.setMiddleinitial(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("dn"))) {
			// userDetail.setDn(value);
		} else if (attName.equalsIgnoreCase(dbConfig.getString("directaddress"))) {
			userDetail.setDirectAddress(value);
		}*/
	}

	public void getUserDetail() throws Exception {

	/*	// String xmlRecords = readFileAsString("D:\\Download\\userinfo.xml");
		User userDetail = new User(new UserId(Long.parseLong(dbConfig.getProperty(SharedConstants.dashCommunityId))));

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(this.value));

		Document doc = db.parse(is);
		NodeList nodes = doc.getElementsByTagName("userdetails");

		Element element = (Element) nodes.item(0);
		NodeList nameList = element.getElementsByTagName("attribute");

		String attName = null;
		String attValue = null;

		for (int j = 0; j < nameList.getLength(); j++) {
			Node cNode = nameList.item(j);
			if (cNode.hasAttributes()) {
				NamedNodeMap nodeMap = cNode.getAttributes();
				attName = nodeMap.item(0).getNodeValue();
				// System.out.println("\nName : " +
				// nodeMap.item(0).getNodeValue());
			}

			if (cNode.getNodeType() == Node.ELEMENT_NODE) {
				Element ele = (Element) cNode;
				attValue = ele.getTextContent();
				// System.out.println("\n value : " + ele.getTextContent());
			}
			setValueToBean(userDetail, attName, attValue, dbConfig);
		}
		checkUserValidation(userDetail);
		return userDetail;
            */
	}

	private void checkUserValidation() throws Exception {
	/*	// unused-todo Auto-generated method stub
		if (userDetail.getAccessLevel() == null) {
			throw new Exception("AccessLevel attribute is null or blank for current user");
		} else if (userDetail.getDirectAddress() == null) {
			throw new Exception("DirectAddress attribute is null or blank for current user");
		} else if (userDetail.getEmail() == null) {
			throw new Exception("Email attribute is null or blank for current user");
		} else if (userDetail.getRole() == null) {
			throw new Exception("Role attribute is null or blank for current user");
		} else if (userDetail.getLastUpdateDate() == null) {
			throw new Exception("LastUpdateDate attribute is null or blank for current user");
		}*/

	}

	public void getOrganizationDetail() throws Exception {
		// String xmlRecords = readFileAsString("D:\\Download\\userinfo.xml");

	/*	Organization organization = new Organization(new OrganizationId(Long.parseLong(dbConfig
				.getProperty(SharedConstants.dashCommunityId))));

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(this.value));

		Document doc = db.parse(is);
		NodeList nodes = doc.getElementsByTagName("userdetails");

		Element element = (Element) nodes.item(0);
		NodeList nameList = element.getElementsByTagName("attribute");

		String attName = null;
		String attValue = null;

		for (int j = 0; j < nameList.getLength(); j++) {
			Node cNode = nameList.item(j);
			if (cNode.hasAttributes()) {
				NamedNodeMap nodeMap = cNode.getAttributes();
				attName = nodeMap.item(0).getNodeValue();
				// System.out.println("\nName : " +
				// nodeMap.item(0).getNodeValue());
			}

			if (cNode.getNodeType() == Node.ELEMENT_NODE) {
				Element ele = (Element) cNode;
				attValue = ele.getTextContent();
				// System.out.println("\n value : " + ele.getTextContent());
			}
			setValueToOrganizationBean(organization, attName, attValue, dbConfig);

		}
		checkOrganizationValidation(organization);
		return organization;*/
	}

	private void checkOrganizationValidation() throws Exception {
	/*	if (organization.getOrganizationName() == null) {
			throw new Exception("OrganizationName attribute is null or blank for current user");
		} else if (organization.getOid() == null) {
			throw new Exception("OrganizationId attribute is null or blank for current user");
		}
                */
	}


}
