/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.audit.authentication;

/**
 *
 * @author Hemant Tyagi
 */
public class BadTokenException extends Exception{
    public BadTokenException(){
        super("BAD TOKEN");
    }
}
