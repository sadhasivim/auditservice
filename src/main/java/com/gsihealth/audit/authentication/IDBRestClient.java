/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.audit.authentication;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/*
 *The purpose of this class is to encapsulate all the functions hitting the open am services.
 *
 */
public class IDBRestClient {

    /*
     * This method authenticates with open am and return a token if goes success
     */
    private static final int TIMEOUT = 10000;

    public String authenticate(String user, String password, String path, String realm) {
        try {
            String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(user, "UTF-8");
            data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
            data += "&" + URLEncoder.encode("uri", "UTF-8") + "=" + "realm=" + realm;

            System.out.println(path + "?" + data);
            String value = getResponse(data, path);
            if (value.contains("token.id")) {
                String response[] = value.split("=");
                return response[1];
            }
        } catch (Exception x) {
            // x.printStackTrace();
            //System.out.println("COULD NOT AUTHENTICATE " + user + " on path " + path);
        }

        return null;

    }

    public String search(String tokenId, String userID, String path) {
        String value = null;
        try {

            String data = URLEncoder.encode("admin", "UTF-8") + "=" + URLEncoder.encode(tokenId, "UTF-8");
            data += "&" + URLEncoder.encode("attributes_names", "UTF-8") + "=" + URLEncoder.encode("uid", "UTF-8");
            data += "&" + URLEncoder.encode("attributes_values_uid", "UTF-8") + "=" + URLEncoder.encode(userID, "UTF-8");

            value = getResponse(data, path);

        } catch (Exception x) {
            // x.printStackTrace();
            System.out.println("COULD NOT Search " + userID + " on path " + path);
        }

        return value;
    }

    /*
     * This method is used to change the password of user in open am
     */
    public void changePassword(String tokenId, String userID, String path, String attributeName, String attributeValue) throws Exception {

        String data = URLEncoder.encode("admin", "UTF-8") + "=" + URLEncoder.encode(tokenId, "UTF-8");
        data += "&" + URLEncoder.encode("identity_name", "UTF-8") + "=" + URLEncoder.encode(userID, "UTF-8");
        data += "&" + URLEncoder.encode("identity_attribute_names", "UTF-8") + "=" + URLEncoder.encode(attributeName, "UTF-8");
        data += "&" + URLEncoder.encode("identity_attribute_values_" + attributeName, "UTF-8") + "="
                + URLEncoder.encode(attributeValue, "UTF-8");

        String value = getResponse(data, path);
        System.out.println(value);

    }

    /*
     * This method id used to fetch the user information on the basis of open am
     * token.
     */
    public String retrieve(String token, String path) throws Exception {

        String data = "subjectid=" + token;
        System.out.println("data for directaddress : "+data);
        System.out.println("path for directaddress : "+path);
        String value = getResponse(data, path);
        //System.out.println(value);

        return value;

    }

    /*
     * This method is used to validate the open am token
     */
    public boolean validateToken(String token, String path) throws Exception {

        boolean tokenValid = false;

        String data = "tokenid=" + token;

        String value = getResponse(data, path);

        System.out.println("value is:" + value);
        tokenValid = value.equals("boolean=true");
        return tokenValid;

    }

    /*
     * This method is used to check the authorization of user who has
     * authentication
     */
    public boolean authorize(String authUrl, String uri, String token) {

        try {

            String data = URLEncoder.encode("uri", "UTF-8") + "=" + URLEncoder.encode(uri, "UTF-8");
            data += "&" + URLEncoder.encode("subjectid", "UTF-8") + "=" + URLEncoder.encode(token, "UTF-8");

            String value = getResponse(data, authUrl);

            if (value.contains("boolean")) {
                String response[] = value.split("=");
                if (response[1].equals("true")) {
                    return true;
                } else {
                    return false;
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return false;

    }

    /*
     * This method id used to logout the user from open am
     */
    public String logout(String token, String path) {

        try {

            String data = "subjectid=" + token;

            String value = getResponse(data, path);

            return value;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;

    }

    private String getResponse(String data, String path) throws Exception {
        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(path);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(TIMEOUT);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            // Get the response
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line;
            StringBuilder value = new StringBuilder("");
            while ((line = rd.readLine()) != null) {

                value.append(line);

            }

            return value.toString();
        } finally {
            if (rd != null) {
                rd.close();
            }

            if (wr != null) {
                wr.close();
            }
            if (conn != null) {
                conn.disconnect();
            }
        }

    }

    public List<String> getRoles(String details) {
        //role=id=patient,ou=group,dc=opensso,dc=java,dc=net
        //attribute.name=uid
        //attribute.value=PentahoAdmin
        ArrayList ret = new ArrayList();
        String[] arr = details.split("userdetails.");
        for (String line : arr) {
            if (line.indexOf("role") == 0) {
                int start = line.indexOf("id=") + 3;
                int end = line.indexOf(',');
                String role = line.substring(start, end);
                ret.add(role);
            }
        }

        return ret;
    }

    public String getUser(String details) {
        //role=id=patient,ou=group,dc=opensso,dc=java,dc=net
        //attribute.name=uid
        //attribute.value=PentahoAdmin
        String ret = null;
        String[] arr = details.split("userdetails.");
        boolean next = false;
        for (String line : arr) {
            if (next) {
                ret = line.substring(16);
                break;
            }
            if (line.equals("attribute.name=uid")) {
                next = true;
            }

        }

        return ret;
    }

    public String getFirstName(String details) {
        //role=id=patient,ou=group,dc=opensso,dc=java,dc=net
        //attribute.name=uid
        //attribute.value=PentahoAdmin
        String ret = null;
        String[] arr = details.split("userdetails.");
        boolean next = false;
        for (String line : arr) {
            if (next) {
                ret = line.substring(16);
                break;
            }
            if (line.equals("attribute.name=givenName")) {
                next = true;
            }

        }

        return ret;
    }

    public String getLastName(String details) {
        //role=id=patient,ou=group,dc=opensso,dc=java,dc=net
        //attribute.name=uid
        //attribute.value=PentahoAdmin
        String ret = null;
        String[] arr = details.split("userdetails.");
        boolean next = false;
        for (String line : arr) {
            if (next) {
                ret = line.substring(16);
                break;
            }
            if (line.equals("attribute.name=sn")) {
                next = true;
            }

        }

        return ret;
    }

    public String getDirectAddress(String details) {
        //role=id=patient,ou=group,dc=opensso,dc=java,dc=net
        //attribute.name=uid
        //attribute.value=PentahoAdmin
        String ret = null;
        String[] arr = details.split("userdetails.");
        boolean next = false;
        for (String line : arr) {
            if (next) {
                ret = line.substring(16);
                break;
            }
            if (line.equals("attribute.name=directAddress")) {
                next = true;
            }

        }

        return ret;
    }

    public String getOrganization(String details) {
        //role=id=patient,ou=group,dc=opensso,dc=java,dc=net
        //attribute.name=uid
        //attribute.value=PentahoAdmin
        String ret = null;
        String[] arr = details.split("userdetails.");
        boolean next = false;
        for (String line : arr) {
            if (next) {
                ret = line.substring(16);
                break;
            }
            if (line.equals("attribute.name=organizationid")) {
                next = true;
            }

        }

        return ret;
    }

//http://10.110.210.63:8080/openam_s952/identity/authenticate.
    /*
     * public static void main(String arg[]) { String token =
     * "AQIC5wM2LY4SfcxzELkAQ6rhoVbr1BHowSNl9x7jO0-dBz8.*AAJTSQACMDEAAlNLABMtNTUyNTY1NDA3MjczMDc2ODY2*"
     * ; String authPath =
     * "http://gsi-idev-app4.gsihealth.local:8080/openam_s952/identity/authenticate"
     * ; String validatePath =
     * "http://gsi-idev-app4.gsihealth.local:8080/openam_s952/identity/isTokenValid"
     * ; String logoutPath =
     * "http://gsi-idev-app4.gsihealth.local:8080/openam_s952/identity/logout";
     * String authorizeUrl =
     * "http://gsi-idev-app4.gsihealth.local:8080/openam_s952/authorize";
     * RestClient client = new RestClient(); //
     * System.out.println(client.authenticate("ajay", "22105012", //
     * "http://meere.internal.forgerock.com:8080/openam_10.1.0/identity/authenticate"
     * )); System.out.println(client.logout(token, logoutPath)); }
    
     http://gh01gheqaapp04.gsihealth.local:8080/openam_s952/identity/search?attributes_names=uid&attributes_values_uid=gsi.qa@gsihealth.com&admin=AQIC5wM2LY4SfcwCXX4EEMMoY8w3UAK_HU5XRxsHvcm16vg.*AAJTSQACMDE.*
    
     */
}
